[[_TOC_]]

# A fuse/FUSE primer

## Getting a fuse dump

To get you started with fuse, fuse dumping is a diagnostic feature that offers a convenient way to get a glimpse to this layer of the stack. As the name suggests, fuse dump is a dump of all the messages that's exchanged between the kernel fuse component (the "fuse module", as often called, although that's a misnomer, as in Linux _module_ refers to a build artifact, not a component boundary) and glusterfs client through _/dev/fuse_. OK, let's add the bits that allow the previous sentence to make sense:

- fuse is a VFS component, like xfs, ext4, etc.
- It's "storage backend" is a process running in userspace, commonly called the fuse server
- There is a communication protocol (referred to FUSE, with capitals) that allows the VFS component and fuse server to communicate
- FUSE by-and-large describes an universal VFS semantics, but it has some Linux-specific quirks. It's implemented for other unixy kernels as well, while the Linux implementation remains the de facto standard. FUSE is versioned in a <major>.<minor> fashion. (Alternative implementations apply various strategies to bridge incompatibilities with Linux VFS and to add their own platform-specific features.) Latest version is 7.31. While the minor is frequently bumped, the major version, 7, has not been changed since fuse can be considered stable.
- the "wire" between kernel component and userland server is _/dev/fuse_. At the time of initialization the server obtains a file descriptor to _/dev/fuse_ and communication with kernel is performed by doing I/O through that fd.
- There is  client and server role. The general scheme is as follows: the kernel component is client, makes requests to server. The userland server, is well, the server, that receives the requests, processes them, and responds back to kernel.
- However, there are exceptions to this scheme:
    - there are special requests that do not require a response;
    - there are special, so-called reverse requests, or inverse notifications, that are initiated by the server and received by the kernel. (For now, it's enough to just know that there are such things.)
- requests follow a _<fuse_in_header><request payload>_ structure, responses follow a _<fuse_out_header><response payload>_ structure. The basic logic is as follows:
    - server does a _sizeof(fuse_in_header)_ sized read to _/dev/fuse_;
    - _fuse_in_header_ has a member that indicates the full size of the request: making use of that, the remainder the of the request is read in; it also carries an unique identifier (`uint64_t unique`) for the request;
    - when processing of the request is done, the server sets up a _fuse_out_header_ instance, that includes the unique to refer back to the request and the result of the action taken, so that 0 indicates success and _negated_ errno indicates error according to the errno being negated [^negated-errno];
    - server writes the _fuse_out_header_ and associated payload to _/dev/fuse_.

The userspace part also has a de facto standard implementation, [libfuse](https://github.com/libfuse/libfuse). There are many alternative implementations, mostly because for non-C languages it might be more straightforward to implement FUSE fully instead of binding to libfuse. And, what's most relevant to us, the glusterfs FUSE implementation is _not_ libfuse based. In fact, it's not hard to implement FUSE (or the relevant part of it) from scratch, if one is willing  to deal with compatibility issues between versions and platform's implementations. Another possible source of confusion: in the glusterfs ecosystem we often talk of the "glusterfs fuse client", albeit it's a FUSE server 🙂 Properly we should say, "the glusterfs client (client in terms of its role within the gluster stack) that implements a FUSE server".

### Exercise: fuse dumping

- mount a glusterfs volume with the `-odump-fuse=<PATH>` option (or, if the client is directly invoked, `--dump-fuse=<PATH>` command line option) (convention: use a PATH with .fuse extension).
- do a few basic operations on the mount. (Probably best is to use a script so that it's listed explicitly what you've done. But remain simple, don't invoke eg. find, rather just do some ls, "echo/cat > ...",  cp, mv, rm, ln (-s), df like stuff on literal paths). Then unmount the volume. You then should find the dumpfile at PATH.
- compile [_parsefuse_](https://github.com/gluster/parsefuse) as of the instructions of the README. [^header-glusterfs]
- feed the dumpfile to the parsefuse executable and study it's output. Try to find out how what you see corresponds to the operations you've performed on the mount.

<!-- footnotes -->
[^negated-errno]: negated errno is a linuxism, the kernel code often applies this convention for function return so that various non-negative values can be used to indicate various success statuses, while negative values indicate error so that the the error condition falls under the errno that is being negated. FUSE follows this convention, but knows of only a single success value, 0.
[^header-glusterfs]: You need to get hold of a certain header file to build _parsefuse_. This is covered in detail in the next chapter. At this point you don't need to know much about it, as we are processing a fuse dump created by glusterfs, for which case the README of _parsefuse_ provides specific simple instructions.

## _parsefuse_ and FUSE

_parsefuse_ source does not have a fixed version of FUSE baked into it; the logic of code just relies on the general format of requests and responses that I described above. However, at build time one has to choose a particular version of FUSE and that will be baked into the resultant executable.

There are two files of descriptive nature that is used during the build of parsefuse:

- the header defining the data structures of the protocol; libfuse and kernel both ship it: in former it's available as _include/fuse_kernel.h_, in latter as _include/uapi/linux/fuse.h_ (distros usually install it to _/usr/include/linux/fuse.h_ by a package called something like kernel-headers or linux-headers);
- a description of the data layout of the request and response payloads, available in parsefuse source tree as [_messages.yaml_](https://github.com/gluster/parsefuse/blob/master/messages.yaml).

For the data layout, it's always the in-tree _messages.yaml_ is that is used — as FUSE is mostly backwards compatible, it's enough to extend this description with new features as they are added (some corner cases break down, but that's handled in code).

The header is what's definitive for the protocol version or variant, so that needs to be explicitly given at build time. (I'm saying "version or variant", because none other than Red Hat bastardized FUSE versioning for RHEL 7 kernels: the FUSE version that the RHEL 7 kernel inherited from upstream is 7.22, but Red Hat backported some 7.24 features to its kernel, so RHEL 7 fuse implements something in between 7.22 and 7.24, without any indication (it just acknowledges itself as 7.22)).

Now how to pick  the right header file when you compile parsefuse? It's specific to deployment: you have to consider 1) the fuse server you use 2) the kernel the file system will be mounted with. At initialization time, fuse performs version negotiation: both kernel and fuse server sends its own FUSE version to the other, and the smaller of the two values (native and peer's FUSE version) is used for the rest of the operation. So the right header to use for dissecting the dump of a given mount is the one with smaller version of the headers the fuse server and the kernel has been compiled against.

At this point you might be rolling your eyes. Why are we talking about the internals of a tool that is only an auxiliary utility in the fuse ecosystem? I tell you why: because when you need a reference for FUSE, these things that we covered here will be your best friends:

- the header file informs you about the data structures used in FUSE;
- _messages.yaml_ informs you about the format of the messages.

### Exercise: find the header

These are the first two lines of the output of _parsefuse_ with a certain fuse dump:

```
2020-09-10T10:34:42.690238854+02:00 "STRA\xce" INIT {Len:56 Opcode:26 Unique:2 Nodeid:0 Uid:0 Gid:0 Pid:0 Padding:0} {Major:7 Minor:31 MaxReadahead:131072 Flags:67108859} 
2020-09-10T10:34:42.69120689+02:00 "STRA\xce" {Len:40 Error:0 Unique:2} {Major:7 Minor:19 MaxReadahead:131072 Flags:1043 MaxBackground:0 CongestionThreshold:0 MaxWrite:131072} 
```

Get hold of the header file which would be appropriate for building _parsefuse_ for this fuse dump.

## Looking into a fuse dump

So FUSE traffic mainly consists of requests and responses. [^req-resp]

Here is an example of two _LOOKUP_ requests and their responses (as presented by _parsefuse_): 

```
2020-09-07T20:05:31.262295561+02:00 "GLUSTER\xf5" LOOKUP {Len:46 Opcode:1 Unique:10 Nodeid:1 Uid:0 Gid:0 Pid:34109 Padding:0} file1 
2020-09-07T20:05:31.263048883+02:00 "GLUSTER\xf5" {Len:16 Error:-2 Unique:10} "" 
2020-09-07T20:05:31.962751388+02:00 "GLUSTER\xf5" LOOKUP {Len:46 Opcode:1 Unique:12 Nodeid:1 Uid:0 Gid:0 Pid:34119 Padding:0} file1 
2020-09-07T20:05:31.963089997+02:00 "GLUSTER\xf5" {Len:144 Error:0 Unique:12} {Nodeid:139677101237800 Generation:0 EntryValid:1 AttrValid:1 EntryValidNsec:0 AttrValidNsec:0 Attr:{Ino:12069965995597228977 Size:5 Blocks:1 Atime:1599501931 Mtime:1599501931 Ctime:1599501954 Atimensec:267606747 Mtimensec:269529557 Ctimensec:433615788 Mode:33188 Nlink:1 Uid:0 Gid:0 Rdev:0 Blksize:131072 Padding:0}}
```

We can detect that two messages are in request-response relation by looking at their Unique: if it's the same, the former is the request, the latter is the response. [^unique-unique] So the first pair are request and response with Unique 10, the second pair are request and response with Unique 12. We can see that the _LOOKUP_ of Unique 10 is responded with failure (the error is errno 2, ENOENT, "No such file or directory"). The _LOOKUP_ of Unique 12 succeeds, the respose carries the entry data. If we are curious what _LOOKUP_-s occur in the fuse session, we can grep the _parsefues_ output for 'LOOKUP'. But what if we are interested only in _successful_ _LOOKUP_-s? Maybe we can apply some tricky grepping, like

```
$ parsefuse somedump.fuse | grep -A1 LOOKUP | grep -B1 Error:0
```

But what if the dump looks like this:

```
2020-09-07T20:05:31.262295561+02:00 "GLUSTER\xf5" LOOKUP {Len:46 Opcode:1 Unique:10 Nodeid:1 Uid:0 Gid:0 Pid:34109 Padding:0} file1 
2020-09-07T20:05:31.263048883+02:00 "GLUSTER\xf5" LOOKUP {Len:46 Opcode:1 Unique:12 Nodeid:1 Uid:0 Gid:0 Pid:34119 Padding:0} file1 
2020-09-07T20:05:31.962751388+02:00 "GLUSTER\xf5" {Len:144 Error:0 Unique:12} {Nodeid:139677101237800 Generation:0 EntryValid:1 AttrValid:1 EntryValidNsec:0 AttrValidNsec:0 Attr:{Ino:12069965995597228977 Size:5 Blocks:1 Atime:1599501931 Mtime:1599501931 Ctime:1599501954 Atimensec:267606747 Mtimensec:269529557 Ctimensec:433615788 Mode:33188 Nlink:1 Uid:0 Gid:0 Rdev:0 Blksize:131072 Padding:0}}
2020-09-07T20:05:31.963089997+02:00 "GLUSTER\xf5" {Len:16 Error:-2 Unique:10} ""
```

ie. requests and responses are interleaved? This can occur under concurrent load with a multithreaded fuse server. The _parsefuse_ output of this format is not suitable for grepping. How could we help that? Well, we could use grep if request-response pairs would occur in the same line: then we could again use grep along this line: `grep 'LOOKUP.*Error:0'`.  So maybe we could tweak _parsefuse_ so that optionally it gives such an output? Or maybe it already has such a feature? Well, it has, not exactly this feature, rather something much more powerful: _JSON output_. If we add `-format=json` to the _parsefuse_ invocation (should occur before the fuse dump argument), the first excerpt will look like:

```
{"Truncated":false,"Msg":[["2020-09-07T20:05:31.262295561+02:00","\"GLUSTER\\xf5\""],"LOOKUP",{"Len":46,"Opcode":1,"Unique":10,"Nodeid":1,"Uid":0,"Gid":0,"Pid":34109,"Padding":0},["file1"]]}
{"Truncated":false,"Msg":[["2020-09-07T20:05:31.263048883+02:00","\"GLUSTER\\xf5\""],{"Len":16,"Error":-2,"Unique":10},[""]]}
{"Truncated":false,"Msg":[["2020-09-07T20:05:31.962751388+02:00","\"GLUSTER\\xf5\""],"LOOKUP",{"Len":46,"Opcode":1,"Unique":12,"Nodeid":1,"Uid":0,"Gid":0,"Pid":34119,"Padding":0},["file1"]]}
{"Truncated":false,"Msg":[["2020-09-07T20:05:31.963089997+02:00","\"GLUSTER\\xf5\""],{"Len":144,"Error":0,"Unique":12},[{"Nodeid":139677101237800,"Generation":0,"EntryValid":1,"AttrValid":1,"EntryValidNsec":0,"AttrValidNsec":0,"Attr":{"Ino":12069965995597228977,"Size":5,"Blocks":1,"Atime":1599501931,"Mtime":1599501931,"Ctime":1599501954,"Atimensec":267606747,"Mtimensec":269529557,"Ctimensec":433615788,"Mode":33188,"Nlink":1,"Uid":0,"Gid":0,"Rdev":0,"Blksize":131072,"Padding":0}}]]}
```

At first glance we get the impression that it's same as the previous rendering just fuzzed with some line noise. 

However, JSON is a standardized format, so there is a rich plethora of tools to do various things with it. Even if we just look at the task of  highlighting it for readability, that's still a rich plethora of tools that can do this for us: [^termshot-utils]

![Termshot](assets/termshot.png)

That's helpful, but we bet on another task JSON makes possible: _rearrangement_ of data. It's easy to code up a script that reads in the JSON rendered fuse dump, matches requests with responses and emits them in one request-response pair in one line fashion. The parsefuse project provides such a utility: [_fusedumppairup.rb_](https://github.com/gluster/parsefuse/tree/master/utils). Filtering the above JSON through it, we get:

```json
{"UNIQUE":10,"OP":"LOOKUP","REQUEST":[["2020-09-07T20:05:31.262295561+02:00","\"GLUSTER\\xf5\""],{"Len":46,"Opcode":1,"Unique":10,"Nodeid":1,"Uid":0,"Gid":0,"Pid":34109,"Padding":0},["file1"]],"RESPONSE":[["2020-09-07T20:05:31.263048883+02:00","\"GLUSTER\\xf5\""],{"Len":16,"Error":-2,"Unique":10},[""]],"Truncated":false}
{"UNIQUE":12,"OP":"LOOKUP","REQUEST":[["2020-09-07T20:05:31.962751388+02:00","\"GLUSTER\\xf5\""],{"Len":46,"Opcode":1,"Unique":12,"Nodeid":1,"Uid":0,"Gid":0,"Pid":34119,"Padding":0},["file1"]],"RESPONSE":[["2020-09-07T20:05:31.963089997+02:00","\"GLUSTER\\xf5\""],{"Len":144,"Error":0,"Unique":12},[{"Nodeid":139677101237800,"Generation":0,"EntryValid":1,"AttrValid":1,"EntryValidNsec":0,"AttrValidNsec":0,"Attr":{"Ino":12069965995597228977,"Size":5,"Blocks":1,"Atime":1599501931,"Mtime":1599501931,"Ctime":1599501954,"Atimensec":267606747,"Mtimensec":269529557,"Ctimensec":433615788,"Mode":33188,"Nlink":1,"Uid":0,"Gid":0,"Rdev":0,"Blksize":131072,"Padding":0}}]],"Truncated":false}
```

Now this can be grepped for `LOOKUP.*Error.:0` for good (just beware to adjust the regex to accommodate the quote characters brought in by JSON). However, when to make sense of this data, you might find it's good for Unix style line oriented text processing, but a bit too dense for the eyes. One way to help this is to extract the relevant bits by further JSON processing. This is what you'll need to try in the following exercise.

### Exercise: data extraction

Put together a script that extracts fields from _fusedumppairup.rb_ output, along the following template: `<OP>[<UNIQUE>] -> <Error>`. So for the above excerpt, your script should give:

```
LOOKUP[10] -> -2
LOOKUP[12] -> 0
```

Try to avoid ad-hoc text mangling, use instead a JSON aware tool. Whichever scripting language — Python, Ruby, Javascript (NodeJS), Lua, ...? — is the one that feels most comfortable to you, it will provide a simple JSON API that makes this easy. "Scripting" here basically boils down to "strong dynamic typing", ie. values have associated types (Unix shells are ruled out), but variables don't. This makes easy to represent the data structure described by JSON in the semantics of the language. You can also use compiled (typically, strongly statically typed) languages, but the heterogeneous character of JSON values will cost some boilerplate to deal with JSON in such languages. Try to make the code succinct (a "one-liner" — although eg. in Python you'll have hard time if you mean it...). If you don't feel any scripting language comfortable, I suggest to turn to _jq_ that was already mentioned among highlighters. Also, if your favorite scripting language is _too_ comfortable and accomplishing this exercise with it would keep you in your comfort zone, consider using _jq_, which presents its own domain specific language to process JSON.

<!-- footnotes -->
[^req-resp]: In this chapter we restrict our investigation to message types that work in request-response fashion.
[^unique-unique]: In the Linux implementation Unique values are truly unique, that is, once they have occurred in a fuse session, they don't occur later on. However, this is not a requirement. The general condition kernels' fuse drivers need to guarantee is that once a request is sent with a given Unique value, no other request is sent with the same Unique value until first such receives a response. So looking at the message sequence in a fuse dump, strictly speaking, in the subsequence of messages with given Unique, the adjacent pairs are in request-response relation.
[^termshot-utils]: The projects providing the tools used are: [Pygments](https://pygments.org/), [Colout](http://nojhan.github.com/colout/), [Bat](https://github.com/sharkdp/bat), [Rouge](http://rouge.jneen.net/), [Coderay](http://coderay.rubychan.de), [Vimpager](https://github.com/rkitover/vimpager), [Jq](https://stedolan.github.io/jq/)

## Looking into a fuse dump, continued.

Another thing that we can do with JSON is to _reformat_ — don't change the data structure, but change the presentation. It's a surprisingly under-explored field of JSON processing: most tools do either fully collapse JSON into a single line (eliminating all whitespace between tokens), or fully expand it. The paired up excerpt will expand to this if you pass it through _jq_ without any options or arguments:

```json
{
  "UNIQUE": 10,
  "OP": "LOOKUP",
  "REQUEST": [
    [
      "2020-09-07T20:05:31.262295561+02:00",
      "\"GLUSTER\\xf5\""
    ],
    {
      "Len": 46,
      "Opcode": 1,
      "Unique": 10,
      "Nodeid": 1,
      "Uid": 0,
      "Gid": 0,
      "Pid": 34109,
      "Padding": 0
    },
    [
      "file1"
    ]
  ],
  "RESPONSE": [
    [
      "2020-09-07T20:05:31.263048883+02:00",
      "\"GLUSTER\\xf5\""
    ],
    {
      "Len": 16,
      "Error": -2,
      "Unique": 10
    },
    [
      ""
    ]
  ],
  "Truncated": false
}
{
  "UNIQUE": 12,
  "OP": "LOOKUP",
  "REQUEST": [
    [
      "2020-09-07T20:05:31.962751388+02:00",
      "\"GLUSTER\\xf5\""
    ],
    {
      "Len": 46,
      "Opcode": 1,
      "Unique": 12,
      "Nodeid": 1,
      "Uid": 0,
      "Gid": 0,
      "Pid": 34119,
      "Padding": 0
    },
    [
      "file1"
    ]
  ],
  "RESPONSE": [
    [
      "2020-09-07T20:05:31.963089997+02:00",
      "\"GLUSTER\\xf5\""
    ],
    {
      "Len": 144,
      "Error": 0,
      "Unique": 12
    },
    [
      {
        "Nodeid": 139677101237800,
        "Generation": 0,
        "EntryValid": 1,
        "AttrValid": 1,
        "EntryValidNsec": 0,
        "AttrValidNsec": 0,
        "Attr": {
          "Ino": 12069965995597228000,
          "Size": 5,
          "Blocks": 1,
          "Atime": 1599501931,
          "Mtime": 1599501931,
          "Ctime": 1599501954,
          "Atimensec": 267606747,
          "Mtimensec": 269529557,
          "Ctimensec": 433615788,
          "Mode": 33188,
          "Nlink": 1,
          "Uid": 0,
          "Gid": 0,
          "Rdev": 0,
          "Blksize": 131072,
          "Padding": 0
        }
      }
    ]
  ],
  "Truncated": false
}
```

If the original data layout was too dense, now this is too sparse (imagine you have to look at not just two messages, but 20 or 50, which is a reasonable range after a properly narrowed down filtering). What would be better is a partial expansion (or partial collapsion). One such tool that can do this is [underscore-cli](https://github.com/ddopson/underscore-cli) — it collapses those subtrees that fit on the terminal collapsed. [^json-tree] However, this way the logic driving collapsion is still in the remit of the tool and does not reflect the user's intent to gain a particular view of the tree. Hence yours truly came up with a JSON filter that puts the control over collapsion to the hands of the user. There are two strategies of collapsion are offered:

- Collapsing subtrees at depth _d_: the depth of a point in tree is the length of the path leading there from the tree root. We'll collapse all subtrees whose root is at least at depth _d_ in the tree.
- Collapsing subtrees up to height _h_: the height of a tree is the maximum length of its paths. We'll collapse all subtrees which are not higher than _h_.

The tool that implements these strategies is [jsonlim](https://github.com/csabahenk/jsonlim). It sports the `-d/--depth` and `-h/--height` options that implement the above strategies. Here is how the paired LOOKUP sample looks when filtered through `jsonlim.rb -d 2 -f jsonl` (the `-f jsonl` part indicates that input lines are well-formed JSON, not the entire input):

```json
[
    {
        "UNIQUE": 10,
        "OP": "LOOKUP",
        "REQUEST": [["2020-09-07T20:05:31.262295561+02:00","\"GLUSTER\\xf5\""],{"Len":46,"Opcode":1,"Unique":10,"Nodeid":1,"Uid":0,"Gid":0,"Pid":34109,"Padding":0},["file1"]],
        "RESPONSE": [["2020-09-07T20:05:31.263048883+02:00","\"GLUSTER\\xf5\""],{"Len":16,"Error":-2,"Unique":10},[""]],
        "Truncated": false
    },
    {
        "UNIQUE": 12,
        "OP": "LOOKUP",
        "REQUEST": [["2020-09-07T20:05:31.962751388+02:00","\"GLUSTER\\xf5\""],{"Len":46,"Opcode":1,"Unique":12,"Nodeid":1,"Uid":0,"Gid":0,"Pid":34119,"Padding":0},["file1"]],
        "RESPONSE": [["2020-09-07T20:05:31.963089997+02:00","\"GLUSTER\\xf5\""],{"Len":144,"Error":0,"Unique":12},[{"Nodeid":139677101237800,"Generation":0,"EntryValid":1,"AttrValid":1,"EntryValidNsec":0,"AttrValidNsec":0,"Attr":{"Ino":12069965995597228977,"Size":5,"Blocks":1,"Atime":1599501931,"Mtime":1599501931,"Ctime":1599501954,"Atimensec":267606747,"Mtimensec":269529557,"Ctimensec":433615788,"Mode":33188,"Nlink":1,"Uid":0,"Gid":0,"Rdev":0,"Blksize":131072,"Padding":0}}]],
        "Truncated": false
    }
]
```

What typically happens is that after an initial filtering of the fuse dump we still have a fairly large chunk of data, in which the relevant bits of information — particular file names, node ids an filehandles are still quite scarce. So instead of (or besides) the general highlighting discussed in the previous section we'll prefer highlighting these particular values. If you use Vim (or its derivatives), you can use the [highlights plugin](https://vim.fandom.com/wiki/Highlight_multiple_words) for this end. The following example highlights the rename history of a file:

![Vim highlight](assets/vim-highlight-rename.png)


The following highlight groups were defined (output of the `:Highlight` command; also, to recreate it, you need to use this command, eg. `:Highlight 7 RENAME`):

```
Highlight groups and patterns:
hl7 RENAME
hl6 },"[^"]\+"
hl5 ,"[^"]\+"\]\]
```

which is ad-hoc (relies on `"` not occurring in the names and the delimiter patterns surrounding the words that we want to be highlighted), but does the job.

The following exercises will ask you to find certain information in sample fuse dumps. They were taken in the same environment as the one cited in [Exercise: find the header](#exercise-find-the-header), so the header you found there will be suitable for parsing these dumps. There is one more technicality involved.

The exercises will require to look at  _READ_/_WRITE_ payloads. If you take the default human readable output of _parsefuse_, _READ_/_WRITE_ payloads will be shown as escaped-quoted strings. However, if you use the JSON output of _parsefuse_, you'll face the fact that the JSON spec does not standardize the representation of binary (non-printable) byte sequences, therefore JSON serializers have to make up their mind about it on their own. The JSON serializer used in _parsefuse_ (the one of Go stdlib) has made the choice to dump byte sequences as base64 encoded strings, so you can't see what is the payload by naked eye. [^fuse-buf-parts] [^fuse-payload-irl] To your help, here is a one-liner that replaces the base64-encoded _READ_/_WRITE_ payloads of paired up JSON format with escaped-quoted string representation:

```shell
ruby -rjson -rbase64 -ne 'r=JSON.load($_); pl,i=case r["OP"]; when "WRITE"; [r["REQUEST"][2],1]; when "READ"; [r["RESPONSE"][2],0]; end; if pl; pl[i]=Base64.decode64(pl[i]); end; puts r.to_json'
```

### Exercise: find out the path

In the fuse dump [find-the-path.fuse](assets/find-the-path.fuse) you'll find that a file is created with the content `jam`. What is the path of this file?

### Exercise: reconstruct the _READ_ payloads

In the fuse dump [reconstruct-read.fuse](assets/reconstruct-read.fuse) you'll find a number of _READ_ messages whose payload has been concealed (overwritten by a sequence of `X`-es). Following through the FUSE session, reconstruct the _READ_ payloads!

<!-- footnotes -->
[^json-tree]: The data structure represented by JSON can be described mathematically as a tree where nodes are scalars (strings, floats, Booleans, null, and ROOT — a singleton indicating the tree root), and edges are labelled with strings and nonnegative integers so that all outgoing edges (edges that lead away from ROOT) bear the same type of label, and if the labels are integers they form a successive sequence starting from 0.
[^fuse-buf-parts]: This convention is used for all message parts indicated as _"buf"_ in _messages.yaml_
[^fuse-payload-irl]: In practical situations this is rarely an issue, as metadata consistency issues are much more prevalent than data corruption issues.

## Footnotes
